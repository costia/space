﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour {
	public GameObject[] mPlanet;
	float mMaxForce=10.0f;
    public Rigidbody rigidbody;

    // Use this for initialization
    void Start () {
        rigidbody = new Rigidbody();


    }
	
	// Update is called once per frame
	void Update () {
		float speed=1.0f;
		if (Input.GetKey(KeyCode.A)){
			this.transform.Rotate(Vector3.up,-speed,Space.Self);
		}
		if (Input.GetKey(KeyCode.D)){
			this.transform.Rotate(Vector3.up,speed,Space.Self);
		}
		if (Input.GetKey(KeyCode.W)){
			this.transform.Rotate(Vector3.right,-speed,Space.Self);
		}
		if (Input.GetKey(KeyCode.S)){
			this.transform.Rotate(Vector3.right,speed,Space.Self);
		}
		if (Input.GetKey(KeyCode.Space)){
			rigidbody.AddForce(10.0f*transform.forward);
		}
		if (Input.GetKey(KeyCode.C)){
			rigidbody.AddForce(-10.0f*transform.forward);
		}
	}
	
	void FixedUpdate(){
		float forceMag=50.0f;
		for (int i=0;i<mPlanet.Length;i++){
			Vector3 force=mPlanet[i].transform.position-transform.position;
			float R=Mathf.Sqrt(force.sqrMagnitude);
			force.Normalize();
			if (forceMag/R<mMaxForce){
				force=force*forceMag/R;
			}else{
				force=force*mMaxForce;
			}
			rigidbody.AddForce(force);	
		}
		
	}
}
