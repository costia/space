﻿using UnityEngine;
using System.Collections;

public class BuildWorld : MonoBehaviour {
	
	public GameObject mPrefab;
	GameObject[,,] mPlanet;
	int size=15;
	// Use this for initialization
	void Start () {
		//mPlanet=new GameObject[size*4,size*4,size*4];
		for (int i=size;i<size*3;i++){
			for (int j=size;j<size*3;j++){
				for (int k=size;k<size*3;k++){
					float dist=Mathf.Pow(size*2-i,2)+Mathf.Pow(size*2-j,2)+Mathf.Pow(size*2-k,2);
					dist=Mathf.Sqrt(dist);
					if ((dist<size)&&(dist>size-2)){
						Vector3 pos=new Vector3(i-size*2,j-size*2,k-size*2)+transform.position;
						//mPlanet[i,j,k]=(GameObject)
							GameObject tmp=(GameObject)Instantiate(mPrefab,pos,Quaternion.identity);
						tmp.transform.parent=transform;
					}
				}
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
